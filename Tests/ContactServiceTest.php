<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
require_once __DIR__ . "/../src/Services/ContactService.php";


final class ContactServiceTest extends TestCase
{
    private $contactService;

    protected function setUp(): void
    {
        $this->contactService = new ContactService();
    }



    public function testAddContactInvalidFirstname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid firstname.");
        $contact = new Contact(null, 'OHIN', '2002-03-29');
        $this->contactService->addContact($contact);
    }

    public function testAddContactInvalidLastname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid lastname.");
        $contact = new Contact('Elvis', null, '2002-03-29');
        $this->contactService->addContact($contact);
    }

    public function testAddContactWithEmptyFirstname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Firstname cannot be empty.");

        $contact = new Contact(' ', 'OHIN', '2002-03-29');
        $this->contactService->addContact($contact);
    }

    public function testAddContactWithEmptyLastname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Lastname cannot be empty.");

        $contact = new Contact('Elvis', ' ', '2002-03-29');
        $this->contactService->addContact($contact);
    }

    public function testAddContactWithEmptyBirthday()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Birthday cannot be empty.");

        $contact = new Contact('Elvis', 'OHIN', ' ');
        $this->contactService->addContact($contact);
    }

    public function testAddContactInvalidDate()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid birthday.");
        $contact = new Contact('Elvis', 'OHIN', null);
        $this->contactService->addContact($contact);
    }

    public function testAddContactWithInvalidBirthdayFormat()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid birthday format. Expected format: YYYY-MM-DD.");

        $contact = new Contact('Elvis', 'OHIN', 'invalid-date');
        $this->contactService->addContact($contact);
    }

    public function testAddContactWithFutureBirthday()
    {
        $this->expectException(InvalidArgumentException::class);
        $futureDate = (new DateTime('+1 year'))->format('Y-m-d');
        $expectedMessage = "Birthday cannot be in the future.";

        $this->expectExceptionMessage($expectedMessage);

        $contact = new Contact('Elvis', 'OHIN', $futureDate);
        $this->contactService->addContact($contact);
    }

    public function testFindByIdWithValidId()
    {
        $contact = new Contact('Elvis', 'OHIN', '2002-03-29');
        $newContact = $this->contactService->addContact($contact);
        $foundContact = $this->contactService->findById($newContact->getId());
        $this->assertNotNull($foundContact);
        $this->assertSame($contact->getFirstname(), $foundContact->getFirstname());
        $this->assertSame($contact->getLastname(), $foundContact->getLastname());
        $this->assertSame($contact->getBirthday(), $foundContact->getBirthday());
    }

    public function testFindByIdWithNullId()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("ID is required.");

        $this->contactService->findById(null);
    }

    public function testFindByIdWithEmptyStringId()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("ID is cannot be empty.");

        $this->contactService->findById(' ');
    }


    public function testFindByIdWithInvalidId()
    {
        $foundContact = $this->contactService->findById(-1);
        $this->assertNull($foundContact);
    }

    public function testFindByIdWithNonExistentId()
    {
        $foundContact = $this->contactService->findById(99*99*99*99*99);
        $this->assertNull($foundContact);
    }

    public function testUpdateContactSuccessfully()
    {
        $contact = new Contact('Elvis', 'OHIN', '2002-03-29');
        $newContact = $this->contactService->addContact($contact);
        $foundContact = $this->contactService->findById($newContact->getId());
        $foundContact->setLastname("Doe");
        $foundContact->setFirstname("Jane");
        $foundContact->setBirthday("1990-01-01");
        $result = $this->contactService->updateContact($foundContact);
        $foundNewContact = $this->contactService->findById($newContact->getId());
        $this->assertTrue($result);
        $this->assertSame("Jane", $foundNewContact->getFirstname());
        $this->assertSame("Doe", $foundNewContact->getLastname());
        $this->assertSame("1990-01-01", $foundNewContact->getBirthday());
    }
    public function testUpdateContactWithoutId()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("ID is required for update.");
        $contact = new Contact('Elvis', 'OHIN', '2002-03-29');
        $this->contactService->updateContact($contact);
    }

    public function testUpdateContactNotFound()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Not found.");
        $contact = new Contact('Elvis', 'OHIN', '2002-03-29');
        $contact->setId(9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9);
        $this->contactService->updateContact($contact);
    }

    public function testUpdateContactInvalidFirstname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Firstname is required for update.");
        $contact = new Contact('Elvis', 'OHIN', '2002-03-29');
        $newContact = $this->contactService->addContact($contact);
        $foundContact = $this->contactService->findById($newContact->getId());
        $foundContact->setLastname("Doe");
        $foundContact->setFirstname(null);
        $foundContact->setBirthday("1990-01-01");
        $this->contactService->updateContact($foundContact);
    }

    public function testUpdateContactInvalidLastname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Lastname is required for update.");
        $contact = new Contact('Elvis', 'OHIN', '2002-03-29');
        $newContact = $this->contactService->addContact($contact);
        $foundContact = $this->contactService->findById($newContact->getId());
        $foundContact->setLastname(null);
        $foundContact->setFirstname("Jane");
        $foundContact->setBirthday("1990-01-01");
        $this->contactService->updateContact($foundContact);
    }

    public function testUpdateContactWithEmptyFirstname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Firstname cannot be empty.");
        $contact = new Contact('Elvis', 'OHIN', '2002-03-29');
        $newContact = $this->contactService->addContact($contact);
        $foundContact = $this->contactService->findById($newContact->getId());
        $foundContact->setLastname("Doe");
        $foundContact->setFirstname(" ");
        $foundContact->setBirthday("1990-01-01");
        $this->contactService->updateContact($foundContact);
    }

    public function testUpdateContactWithEmptyLastname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Lastname cannot be empty.");
        $contact = new Contact('Elvis', 'OHIN', '2002-03-29');
        $newContact = $this->contactService->addContact($contact);
        $foundContact = $this->contactService->findById($newContact->getId());
        $foundContact->setLastname(" ");
        $foundContact->setFirstname("John");
        $foundContact->setBirthday("1990-01-01");
        $this->contactService->updateContact($foundContact);
    }

    public function testUpdateContactWithEmptyBirthday()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Birthday cannot be empty.");
        $contact = new Contact('Elvis', 'OHIN', '2002-03-29');
        $newContact = $this->contactService->addContact($contact);
        $foundContact = $this->contactService->findById($newContact->getId());
        $foundContact->setLastname("Doe");
        $foundContact->setFirstname("John");
        $foundContact->setBirthday(" ");
        $this->contactService->updateContact($foundContact);
    }

    public function testUpdateContactInvalidDate()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Birthday is required for update.");
        $contact = new Contact('Elvis', 'OHIN', '2002-03-29');
        $newContact = $this->contactService->addContact($contact);
        $foundContact = $this->contactService->findById($newContact->getId());
        $foundContact->setLastname("Joe");
        $foundContact->setFirstname("John");
        $foundContact->setBirthday(null);
        $this->contactService->updateContact($foundContact);
    }

    public function testUpdateContactWithInvalidBirthdayFormat()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid birthday format. Expected format: YYYY-MM-DD.");
        $contact = new Contact('Elvis', 'OHIN', '2002-03-29');
        $newContact = $this->contactService->addContact($contact);
        $foundContact = $this->contactService->findById($newContact->getId());
        $foundContact->setLastname("Joe");
        $foundContact->setFirstname("John");
        $foundContact->setBirthday("invalid");
        $this->contactService->updateContact($foundContact);
    }

    public function testUpdateContactWithFutureBirthday()
    {
        $this->expectException(InvalidArgumentException::class);
        $futureDate = (new DateTime('+1 year'))->format('Y-m-d');
        $expectedMessage = "Birthday cannot be in the future.";
        $this->expectExceptionMessage($expectedMessage);
        $contact = new Contact('Elvis', 'OHIN', $futureDate);
        $newContact = $this->contactService->addContact($contact);
        $foundContact = $this->contactService->findById($newContact->getId());
        $foundContact->setLastname("Joe");
        $foundContact->setFirstname("John");
        $foundContact->setBirthday($futureDate);
        $this->contactService->updateContact($foundContact);
    }
    public function testDeleteContactWithValidId()
    {
        $contact = new Contact('Elvis', 'OHIN', '2002-03-29');
        $newContact = $this->contactService->addContact($contact);
        $result = $this->contactService->deleteContact($newContact->getId());
        $this->assertTrue($result);
    }

    public function testDeleteContactWithNullId()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("ID is required for deletion.");
        $this->contactService->deleteContact(null);
    }

    public function testDeleteContactNotFound()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Not found.");
        $id = 9*9*6*69*9*99*9*9*9*9*9*9*9*9*9*9;
        $this->contactService->deleteContact($id);
    }

    public function testFindAll()
    {
        $contacts = $this->contactService->findAll();
        $this->assertIsArray($contacts);
    }
}
