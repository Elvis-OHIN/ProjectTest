<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
require_once __DIR__ . "/../src/Services/ContactService.php";

final class IntegrationTest extends TestCase
{
    private $contactService;

    protected function setUp(): void
    {
        $this->contactService = new ContactService();
    }

    public function testCanAddContact(): void
    {
        $contact = new Contact('Elvis', 'OHIN', '2002-03-29');
        $newContact = $this->contactService->addContact($contact);
        $this->assertNotNull($newContact->getId());
        $this->assertSame($contact->getFirstname(), $newContact->getFirstname());
        $this->assertSame($contact->getLastname(), $newContact->getLastname());
        $this->assertSame($contact->getBirthday(), $newContact->getBirthday());
    }
}
