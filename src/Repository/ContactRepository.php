<?php
declare(strict_types=1);

require __DIR__ . "/../Models/Contact.php";
class ContactRepository
{
    private $pdo;

    public function __construct()
    {
        $pathToDb = __DIR__ . '/../database.sqlite';
        $this->pdo = new PDO("sqlite:$pathToDb");
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo->query("CREATE TABLE IF NOT EXISTS contact (
            id            INTEGER         PRIMARY KEY AUTOINCREMENT,
            firstname         VARCHAR( 50 ),
            lastname       VARCHAR( 50 ),
            birthday  DATETIME
        );");
    }

    public function add(Contact $contact): ?int
    {
        $stmt = $this->pdo->prepare('INSERT INTO contact (firstname, lastname, birthday) VALUES (?, ?, ?)');
        $stmt->execute([$contact->getFirstname(), $contact->getLastname(), $contact->getBirthday()]);
        $id = $this->pdo->lastInsertId();
        $contact->setId((int)$id);
        return $contact->getId();
    }


    public function findById($id): ?Contact
    {
        $stmt = $this->pdo->prepare('SELECT * FROM contact WHERE id = ?');
        $stmt->execute([$id]);
        $row = $stmt->fetch();

        if (!$row) {
            return null;
        }

        $contact = new Contact($row['firstname'], $row['lastname'], $row['birthday']);
        $contact->setId((int)$row['id']);
        return $contact;
    }


    public function findAll(): array
    {
        $stmt = $this->pdo->query('SELECT * FROM contact');
        $contacts = [];
        while ($row = $stmt->fetch()) {
            $contacts[] = new Contact($row['firstname'], $row['lastname'], $row['birthday']);
        }

        return $contacts;
    }

    public function update(Contact $contact): bool
    {
        $stmt = $this->pdo->prepare('UPDATE contact SET firstname = ?, lastname = ?, birthday = ? WHERE id = ?');
        $success = $stmt->execute([$contact->getFirstname(), $contact->getLastname(), $contact->getBirthday(), $contact->getId()]);
        return $success;
    }

    public function delete($id): bool
    {
        $stmt = $this->pdo->prepare('DELETE FROM contact WHERE id = ?');
        return $stmt->execute([$id]);
    }
    public function deleteAll(): bool
    {
        $stmt = $this->pdo->prepare('DELETE FROM contact WHERE id > 0');
        return $stmt->execute();
    }
}
