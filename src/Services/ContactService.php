<?php
declare(strict_types=1);

require __DIR__ . "/../Repository/ContactRepository.php";

class ContactService
{
    private $contactRepository;


    public function __construct()
    {
        $this->contactRepository = new ContactRepository();
    }

    public function addContact(Contact $contact): ?Contact
    {

        if($contact->getFirstname() == null) {
            throw new InvalidArgumentException("Invalid firstname.");
        }

        if($contact->getLastname()  == null) {
            throw new InvalidArgumentException("Invalid lastname.");
        }

        if($contact->getBirthday()  == null){
            throw new InvalidArgumentException("Invalid birthday.");
        }

        if($contact->getFirstname() == ' ') {
            throw new InvalidArgumentException("Firstname cannot be empty.");
        }

        if($contact->getLastname()  == ' ') {
            throw new InvalidArgumentException("Lastname cannot be empty.");
        }

        if($contact->getBirthday()  == ' '){
            throw new InvalidArgumentException("Birthday cannot be empty.");
        }

        if (!$this->isValidDate($contact->getBirthday())) {
            throw new InvalidArgumentException("Invalid birthday format. Expected format: YYYY-MM-DD.");
        }

        if ($this->isFutureDate($contact->getBirthday())) {
            throw new InvalidArgumentException("Birthday cannot be in the future.");
        }

        $idContact = $this->contactRepository->add($contact);
        $newContact = $this->findById($idContact);
        return $newContact;
    }
    public function findAll(): array
    {
        $success = $this->contactRepository->findAll();
        return $success;
    }

    public function findById($id): ?Contact
    {
        if ($id == null) {
            throw new InvalidArgumentException("ID is required.");
        }

        if ($id == ' ') {
            throw new InvalidArgumentException("ID is cannot be empty.");
        }
        $contact = $this->contactRepository->findById($id);
        return $contact;
    }

    public function updateContact(Contact $contact): bool
    {
        if ($contact->getId() == null) {
            throw new InvalidArgumentException("ID is required for update.");
        }

        if($contact->getFirstname() == null) {
            throw new InvalidArgumentException("Firstname is required for update.");
        }

        if($contact->getLastname()  == null) {
            throw new InvalidArgumentException("Lastname is required for update.");
        }

        if($contact->getBirthday()  == null){
            throw new InvalidArgumentException("Birthday is required for update.");
        }
        if ($contact->getId() == ' ') {
            throw new InvalidArgumentException("ID cannot be empty.");
        }

        if($contact->getFirstname() == ' ') {
            throw new InvalidArgumentException("Firstname cannot be empty.");
        }

        if($contact->getLastname()  == ' ') {
            throw new InvalidArgumentException("Lastname cannot be empty.");
        }

        if($contact->getBirthday()  == ' '){
            throw new InvalidArgumentException("Birthday cannot be empty.");
        }

        if (!$this->isValidDate($contact->getBirthday())) {
            throw new InvalidArgumentException("Invalid birthday format. Expected format: YYYY-MM-DD.");
        }

        if ($this->isFutureDate($contact->getBirthday())) {
            throw new InvalidArgumentException("Birthday cannot be in the future.");
        }

        if ($this->findById($contact->getId()) == null) {
            throw new InvalidArgumentException("Not found.");
        }
        $success = $this->contactRepository->update($contact);
        return $success;
    }

    private function isValidDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    private function isFutureDate($date)
    {
        $now = new DateTime();
        $inputDate = new DateTime($date);
        return $inputDate > $now;
    }

    public function deleteContact($id): bool
    {
        if ($id == null) {
            throw new InvalidArgumentException("ID is required for deletion.");
        }

        if ($this->findById($id) == null) {
            throw new InvalidArgumentException("Not found.");
        }

        $success = $this->contactRepository->delete($id);
        return $success;
    }
}
