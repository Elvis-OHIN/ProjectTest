<?php declare(strict_types=1);
class Contact
{
    private $id;

    private $firstname;

    private $lastname;

    private $birthday;

    public function __construct($firstname, $lastname , $birthday) {
        $this->firstname = $firstname;
        $this->lastname  =  $lastname ;
        $this->birthday = $birthday;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getFirstname() {
        return $this->firstname;
    }

    public function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    public function getLastname() {
        return $this->lastname;
    }

    public function setLastname($lastname) {
        $this->lastname = $lastname;
    }
    public function getBirthday() {
        return $this->birthday;
    }

    public function setBirthday($birthday) {
        $this->birthday = $birthday;
    }

}
